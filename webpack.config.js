﻿module.exports = {
mode:"production",
    mode:"development", //不压缩

    // 入口文件，指向app.js
    entry: {
        dist: './sys_fun_es6.js'
    },
    // 出口文件
    output: {
        path: __dirname + '/dist',
        // 文件名，将打包好的导出为bundle.js
        filename: '[name].js'
    },
    module: {
        rules: [
            { "test":/\.m?js$/, use: {loader:"babel-loader", options:{presets:["@babel/preset-env"]}}, exclude:/node_modules|bower_components/}
        ]
    }
}

//
// plugins: [
//     // new webpack.optimize.UglifyJsPlugin({
//     //     compress: {
//     //         warnings: false
//     //     }
//     // }),
//     // new webpack.ProvidePlugin({
//     //     $: 'jquery',
//     //     jQuery: 'jquery',
//     //     'window.jQuery': 'jquery'
//     // })
// ]