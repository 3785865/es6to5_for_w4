
//加载服务端数据 
window.init_DATA = async  function() {
    DATA_myPortal = {}//核心数据对象
    DATA_myPortal.isSysAdmin = await LOAD_layotData_admin_zt6()//是否是管理员
    DATA_myPortal.myPortalConfigValIndex = LOAD_myPortalConfigValIndex()//当前用户的配置值索引
    DATA_myPortal.myPortalNavPage = LOAD_myPortalNavPage()//当前(及初始化)用户所在栏目导航位
    DATA_myPortal.apps = []//所有应用信息列表
    DATA_myPortal.theApp = {}//当前应用信息
    DATA_myPortal.sysUiConfig = await LOAD_sysUiConfig(DATA_myPortal.myPortalConfigValIndex)//系统可配置项
    DATA_myPortal.myPortalConfigVal = await LOAD_myPortalConfigVal(DATA_myPortal.sysUiConfig,DATA_myPortal.myPortalConfigValIndex)//当前用户的配置值
    DATA_myPortal.topNav = await LOAD_topNav()//加载顶级菜单
    DATA_myPortal.sysTools = await LOAD_sysTools()//加载tools菜单
}


//2、 获取root系统布局默认配置项
function LOAD_potalLayout_root() {
    return new Promise(function(resolve){
        $.getJSON(COMMON.url["fui"]+"/fui/system/skin/root/uiconfig.json",function(uiconfig){
            resolve(uiconfig.data)
        })
    })
}


//3、获取当前SKIN布局配置项
function LOAD_potalLayout_skin(skinname){
    return new Promise(function(resolve){
        $.getJSON(COMMON.url["fui"]+"/fui/system/skin/"+skinname+"/uiconfig.json",function(uiconfig){
            resolve(uiconfig.data)
        })
    })
}

//4、获取系统可配置项
async function LOAD_sysUiConfig(myPortalConfigIndex) {
    var root_uiconfig = await LOAD_potalLayout_root();
    var skinname = root_uiconfig.uistyle[myPortalConfigIndex.uistyle].Value
    var skin_uiconfig = await LOAD_potalLayout_skin(skinname);
    //root的和skin的合并，skin级别更高
    return $.extend(root_uiconfig, skin_uiconfig)
}



//全系统第一个运行函数，获取系统界面配置,检测是否是admin以及获取页面访问权限zt6获取登录信息
function LOAD_layotData_admin_zt6(portalInit){
    return new Promise(function(resolve){
        $.ajax({
            type : "GET",
            url :  '/zt6/api/uim/sysadmin?rd='+Math.random(),
            data : {},
            dataType : 'json',
            async : true,
            error : function(xhr, textStatus, errorThrown){
                var sessionstatus = xhr.getResponseHeader("sessionstatus");
                if(sessionstatus == "needauth"){
                    var authUrl = xhr.getResponseHeader("authUrl");
                    $.ajax({
                        type : "GET",
                        url : authUrl,
                        dataType : 'json',
                        async : false,
                        success : function(result){
                            resolve(result.success)
                            //myPortal.isSysAdmin = result.success;

                        },
                        error:function(){
                            //非法操作，去登录页
                            F.showToast("您已退出系统,请重新登陆");
                            setTimeout(function(){
                                window.location.href="../uim";
                            },200)
                        }
                    });
                }
            },
            success : function(result){
                resolve(result.success)
            }
        });
    })
}

//加载顶级导航数据
function LOAD_topNav(){
    return new Promise(function(resolve){
        $.getJSON(FApiMenuTab_Main,function(data){
            resolve(data)
        })
    })
}

//加载工具栏数据
function LOAD_sysTools(){
    return new Promise(function(resolve){
        $.getJSON(FApiMenuTab_Tools,function(data){
            resolve(data)
        })
    })
}